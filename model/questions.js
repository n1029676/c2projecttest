var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var shortid = require('shortid');



var answerSchema = new Schema ({
    _id: {type: String, unique: true, 'default': shortid.generate},
    date: {type: Date, default: Date.now},
    points: {type: Number, default: 0},
    //author: {type:Schema.ObjectID, ref:'User'}, //Need to define User model before this can be used.
    author: String,
    answer: String
});
mongoose.model('Answer', answerSchema);

//creates the schema of a question. All entries are required to match this setup.
var questionSchema = new Schema({
    _id: {type: String, unique: true, 'default': shortid.generate},
    date: {type: Date, default: Date.now},
    points: {type: Number, default: 0},
    author   : String,
    title    : String,
    question : String,
    __v: {type: Number, select: false},
    answers: [answerSchema]
});
mongoose.model('Question', questionSchema);
