var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'KCOMP API' });
});

router.get('/questionView', function (req, res, next) {
  res.render('questionView', { title: 'Question viewer' });
});

module.exports = router;
