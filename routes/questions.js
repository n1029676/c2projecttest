var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
require('../model/questions.js');

//defines Question to be a mongoose model (as defined in model/questions.js).
Question = mongoose.model('Question');
Answer   = mongoose.model('Answer');

//When a browser navigates to /questions/ it is served this request, which lists ALL entries in the questions collection
router.get('/', function(req, res, next) {
  Question.find({}, 'author title question points date',function(err, results) {
    res.json(results);
  });
});

//When a POST is pushed to /questions/ it takes the form variables author and question and inserts it into the collection
router.post('/', function (req, res, next) {
  //actually creates the 'question document'
  var newQuestion = new Question({
    author: req.body.author,
    title: req.body.title,
    question: req.body.question
  });
  //actually saves it to the database
  newQuestion.save();
  //will return the newly created object
  res.json(newQuestion);
});

router.get('/:ID', function (req, res, next) {
  //get a specific question
  Question.find({_id: req.params.ID}, function(err, results) {
    res.json(results);
  });
});


router.post('/:QID/answers', function (req, res, next) {
  //create a new answer for a particular question
  Question.findById(req.params.QID, function (err, question) {
    question.answers.push({answer: req.body.answer});
    question.save(function (err) {
      if (err) {res.send(400)} else {res.json(question)};
    })
  });
});

module.exports = router;
